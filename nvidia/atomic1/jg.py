import reframe as rfm
import reframe.utility.sanity as sn


@rfm.simple_test
class HelloTest(rfm.CompileOnlyRegressionTest):
    atomic = parameter(['-D__GCC_ATOMIC_TEST_AND_SET_TRUEVAL=1'])
    localrc = parameter([True, False])
    # gnuv = parameter(['8.1.0', '8.3.0', 'none'])
    gnuv = parameter(['8.1.0', '8.3.0', '9.3.0', '10.3.0', '11.2.0', 'none'])

    valid_systems = ['*']
    valid_prog_environs = ['*']
    login = True
    # executable = 'echo 9'
    build_system = 'SingleSource'
    sourcepath = '1.cpp'
    prebuild_cmds = [
        'module load nvhpc-nompi/21.9',
        'echo -n CC:', 'CC --version',
        'echo -n nvc++:', 'nvc++ --version',
    ]

    @run_before('compile')
    def set_compiler_flags(self):
        self.build_system.cppflags = ['-w', self.atomic]

    @run_before('compile')
    def set_localrc(self):
        echo = ''
        if not self.localrc:
            echo = 'echo'
        if self.gnuv != 'none':
            self.prebuild_cmds += [
                f'# atomic={self.atomic}',
                f'# gcc/{self.gnuv}',
                (f'{echo} ./makelocalrc /opt/nvidia/hpc_sdk/Linux_x86_64/21.9/compilers/bin -d . '
                f'-gcc /opt/gcc/{self.gnuv}/bin/gcc '
                f'-gpp /opt/gcc/{self.gnuv}/bin/g++ '
                f'-g77 /opt/gcc/{self.gnuv}/bin/gfortran '
                '-cuda 11.4 -stdpar 60 -x')
            ]
            self.variables = {
                'NVLOCALRC': '$PWD/localrc'
            }
        else:
            self.prebuild_cmds += [
                f'# atomic={self.atomic}',
                f'# gcc/{self.gnuv}',
            ]

        self.variables['NVIDIA_PATH'] = '/opt/nvidia/hpc_sdk/Linux_x86_64/21.9'
        # TODO: -std=c++17
        self.postbuild_cmds += [
            '# --------------------------',
            'echo',
            f'nvc++ -w {self.atomic} {self.sourcepath} -o exe2',
        ]

    @sanity_function
    def assert_hello(self):
        return sn.assert_not_found(r'Err', self.stdout)
