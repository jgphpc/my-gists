#{{{ hipcc
- https://docs.lumi-supercomputer.eu/eap/#compiling-hip-code

cd /pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_hipcc/

# NOTE: mygpu -> gfx90a !!!
module load CrayEnv craype-accel-amd-gfx90a \
cray-mpich rocm rocmlibs rocThrust craype-x86-rome

gcc --version
# gcc (SUSE Linux) 7.5.0

hipcc --version
# HIP version: 4.4.21432-f9dccde4
# AMD clang version 13.0.0 (https://github.com/RadeonOpenCompute/llvm-project roc-4.5.2 21432 9bbd96fd1936641cd47defd8022edafd063019d5)

# ml -t
perftools-base/22.06.0
cce/14.0.1
craype/2.7.16
cray-dsmml/0.2.2
cray-libsci/21.08.1.2
PrgEnv-cray/8.3.3
ModuleLabel/label
init-lumi/0.1
libfabric/1.15.0.0
craype-network-ofi
xpmem/2.3.2-2.2_6.13__g93dd7ee.shasta
CrayEnv
craype-accel-amd-gfx90a
cray-mpich/8.1.17
rocm/4.5.2
rocmlibs/4.5.2
rocThrust/4.5.2
craype-x86-rome


# NOTE: mygpu -> gfx90a !!!

hipcc --offload-arch=gfx90a -fPIC -fgpu-rdc -std=c++17 -w -c domain/util.cu
hipcc --offload-arch=gfx90a -fPIC -fgpu-rdc -std=c++17 -w -c domain/test.cu -Iinclude
hipcc --offload-arch=gfx90a -fPIC -fgpu-rdc -shared -dlink util.o test.o -o device_link.o --hip-link

#no: hipcc 

/appl/lumi/SW/CrayEnv/EB/rocm/4.5.2/llvm/bin/clang++ \
src/main.cpp util.o test.o \
device_link.o -o exe -Iinclude -L${ROCM_PATH}/lib -lamdhip64

LD_LIBRARY_PATH=${ROCM_PATH}/hip/lib:$LD_LIBRARY_PATH ldd exe

LD_LIBRARY_PATH=${ROCM_PATH}/hip/lib:$LD_LIBRARY_PATH \
OMP_NUM_THREADS=1 \
srun --gpus 1 -N1 -n1 -c1 -t1 --partition=eap -A project_465000105 ./exe

# "hipErrorNoBinaryForGpu: Unable to find code object for all current devices!"
# BECAUSE --offload-arch=gfx908 is wrong: use --offload-arch=gfx90a !
Results check passed!
#}}}

#{{{ cc
- https://docs.lumi-supercomputer.eu/eap/#compiling-hip-code

cd /pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cc/

# NOTE: mygpu -> gfx90a !!!
module load CrayEnv PrgEnv-cray craype-accel-amd-gfx90a \
cray-mpich rocm rocmlibs rocThrust craype-x86-rome

gcc --version
# gcc (SUSE Linux) 7.5.0

hipcc --version
# HIP version: 4.4.21432-f9dccde4
# AMD clang version 13.0.0 (https://github.com/RadeonOpenCompute/llvm-project roc-4.5.2 21432 9bbd96fd1936641cd47defd8022edafd063019d5)

cc --version
# Cray clang version 14.0.1

# NOTE: mygpu -> gfx90a !!!

cc -x hip -I/appl/lumi/SW/CrayEnv/EB/rocm/4.5.2/hip/include --offload-arch=gfx90a -fPIC -fgpu-rdc -std=c++17 -w -c domain/util.cu
cc -x hip -I/appl/lumi/SW/CrayEnv/EB/rocm/4.5.2/hip/include --offload-arch=gfx90a -fPIC -fgpu-rdc -std=c++17 -w -c domain/test.cu -Iinclude
cc                                                          --offload-arch=gfx90a -fPIC -fgpu-rdc -shared -dlink util.o test.o -o device_link.o --hip-link
## cc src/main.cpp util.o test.o device_link.o -o exe -Iinclude -L${ROCM_PATH}/lib -lamdhip64
# Warning: Ignoring device section hip-amdgcn-amd-amdhsa-gfx908
# ld.lld: error: undefined symbol: __hip_fatbin
# >>> referenced by test.cu
# >>>               test.o:(__hip_fatbin_wrapper)
# clang-14: error: linker command failed with exit code 1 (use -v to see invocation)

cc -Iinclude --offload-arch=gfx90a -fPIC -fgpu-rdc \
src/main.cpp util.o test.o device_link.o -o exe \
--hip-link -L${ROCM_PATH}/hip/lib -lamdhip64

LD_LIBRARY_PATH=${ROCM_PATH}/hip/lib:/appl/lumi/SW/CrayEnv/EB/rocm/5.1.4/hip/lib:$LD_LIBRARY_PATH ldd exe

LD_LIBRARY_PATH=${ROCM_PATH}/hip/lib:/appl/lumi/SW/CrayEnv/EB/rocm/5.1.4/hip/lib:$LD_LIBRARY_PATH \
OMP_NUM_THREADS=1 \
srun --gpus 1 -N1 -n1 -c1 -t1 --partition=eap -A project_465000105 ./exe
## "hipErrorNoBinaryForGpu: Unable to find code object for all current devices!"
# BECAUSE --offload-arch=gfx908 is wrong: use --offload-arch=gfx90a !
Results check passed!
#}}}


#{{{ cmake
# NOTE: mygpu -> gfx90a !!!

cmake -S . -B build_cmake
# -- The CXX compiler identification is GNU 10.3.0
# -- The C compiler identification is GNU 10.3.0
# -- Detecting CXX compiler ABI info
# -- Detecting CXX compiler ABI info - done
# -- Check for working CXX compiler: /opt/cray/pe/gcc/10.3.0/snos/bin/c++ - skipped
# -- Detecting CXX compile features
# -- Detecting CXX compile features - done
# -- Detecting C compiler ABI info
# -- Detecting C compiler ABI info - done
# -- Check for working C compiler: /opt/cray/pe/gcc/10.3.0/snos/bin/gcc - skipped
# -- Detecting C compile features
# -- Detecting C compile features - done
# -- Found HIP: /appl/lumi/SW/CrayEnv/EB/rocm/4.5.2/hip (found version "4.4.21432-f9dccde4")
# -- Found HIP: 4.4.21432-f9dccde4
# -- Looking for a HIP compiler
# -- Looking for a HIP compiler - /appl/lumi/SW/CrayEnv/EB/rocm/4.5.2/llvm/bin/clang++
# -- The HIP compiler identification is Clang 13.0.0
# -- Detecting HIP compiler ABI info
# -- Detecting HIP compiler ABI info - done
# -- Check for working HIP compiler: /appl/lumi/SW/CrayEnv/EB/rocm/4.5.2/llvm/bin/clang++ - skipped
# -- Detecting HIP compile features
# -- Detecting HIP compile features - done
# -- Configuring done
# -- Generating done
# -- Build files have been written to: /pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake


cmake --build build_cmake -v
#{{{ /pfs/lustrep4/users/jepiccin/bin/src/cmake-3.23.1-linux-x86_64/bin/cmake -S/pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112 -B/pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake --check-build-system CMakeFiles/Makefile.cmake 0
/pfs/lustrep4/users/jepiccin/bin/src/cmake-3.23.1-linux-x86_64/bin/cmake -E cmake_progress_start /pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake/CMakeFiles /pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake//CMakeFiles/progress.marks
/usr/bin/gmake  -f CMakeFiles/Makefile2 all
gmake[1]: Entering directory '/pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake'
/usr/bin/gmake  -f domain/CMakeFiles/util_obj.dir/build.make domain/CMakeFiles/util_obj.dir/depend
gmake[2]: Entering directory '/pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake'
cd /pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake && /pfs/lustrep4/users/jepiccin/bin/src/cmake-3.23.1-linux-x86_64/bin/cmake -E cmake_depends "Unix Makefiles" /pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112 /pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/domain /pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake /pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake/domain /pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake/domain/CMakeFiles/util_obj.dir/DependInfo.cmake --color=
Dependee "/pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake/domain/CMakeFiles/util_obj.dir/DependInfo.cmake" is newer than depender "/pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake/domain/CMakeFiles/util_obj.dir/depend.internal".
Dependee "/pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake/domain/CMakeFiles/CMakeDirectoryInformation.cmake" is newer than depender "/pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake/domain/CMakeFiles/util_obj.dir/depend.internal".
Scanning dependencies of target util_obj
gmake[2]: Leaving directory '/pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake'
/usr/bin/gmake  -f domain/CMakeFiles/util_obj.dir/build.make domain/CMakeFiles/util_obj.dir/build
gmake[2]: Entering directory '/pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake'
[ 25%] Building HIP object domain/CMakeFiles/util_obj.dir/util.cu.o
cd /pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake/domain && /appl/lumi/SW/CrayEnv/EB/rocm/4.5.2/llvm/bin/clang++ -D__HIP_ROCclr__=1  -fPIC -fgpu-rdc -w -I/appl/lumi/SW/CrayEnv/EB/rocm/4.5.2/hip/include --gcc-toolchain=/opt/cray/pe/gcc/10.3.0/snos --offload-arch=gfx90a -mllvm -amdgpu-early-inline-all=true -mllvm -amdgpu-function-calls=false -std=gnu++17 -o CMakeFiles/util_obj.dir/util.cu.o -x hip -c /pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/domain/util.cu
gmake[2]: Leaving directory '/pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake'
[ 25%] Built target util_obj
/usr/bin/gmake  -f domain/CMakeFiles/test_obj.dir/build.make domain/CMakeFiles/test_obj.dir/depend
gmake[2]: Entering directory '/pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake'
cd /pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake && /pfs/lustrep4/users/jepiccin/bin/src/cmake-3.23.1-linux-x86_64/bin/cmake -E cmake_depends "Unix Makefiles" /pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112 /pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/domain /pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake /pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake/domain /pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake/domain/CMakeFiles/test_obj.dir/DependInfo.cmake --color=
Dependee "/pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake/domain/CMakeFiles/test_obj.dir/DependInfo.cmake" is newer than depender "/pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake/domain/CMakeFiles/test_obj.dir/depend.internal".
Dependee "/pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake/domain/CMakeFiles/CMakeDirectoryInformation.cmake" is newer than depender "/pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake/domain/CMakeFiles/test_obj.dir/depend.internal".
Scanning dependencies of target test_obj
gmake[2]: Leaving directory '/pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake'
/usr/bin/gmake  -f domain/CMakeFiles/test_obj.dir/build.make domain/CMakeFiles/test_obj.dir/build
gmake[2]: Entering directory '/pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake'
[ 50%] Building HIP object domain/CMakeFiles/test_obj.dir/test.cu.o
cd /pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake/domain && /appl/lumi/SW/CrayEnv/EB/rocm/4.5.2/llvm/bin/clang++ -D__HIP_ROCclr__=1 -I/pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/include -fPIC -fgpu-rdc -w -I/appl/lumi/SW/CrayEnv/EB/rocm/4.5.2/hip/include --gcc-toolchain=/opt/cray/pe/gcc/10.3.0/snos --offload-arch=gfx90a -mllvm -amdgpu-early-inline-all=true -mllvm -amdgpu-function-calls=false -std=gnu++17 -o CMakeFiles/test_obj.dir/test.cu.o -x hip -c /pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/domain/test.cu
gmake[2]: Leaving directory '/pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake'
[ 50%] Built target test_obj
/usr/bin/gmake  -f src/CMakeFiles/myexe-hip.dir/build.make src/CMakeFiles/myexe-hip.dir/depend
gmake[2]: Entering directory '/pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake'
cd /pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake && /pfs/lustrep4/users/jepiccin/bin/src/cmake-3.23.1-linux-x86_64/bin/cmake -E cmake_depends "Unix Makefiles" /pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112 /pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/src /pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake /pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake/src /pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake/src/CMakeFiles/myexe-hip.dir/DependInfo.cmake --color=
Dependee "/pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake/src/CMakeFiles/myexe-hip.dir/DependInfo.cmake" is newer than depender "/pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake/src/CMakeFiles/myexe-hip.dir/depend.internal".
Dependee "/pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake/src/CMakeFiles/CMakeDirectoryInformation.cmake" is newer than depender "/pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake/src/CMakeFiles/myexe-hip.dir/depend.internal".
Scanning dependencies of target myexe-hip
gmake[2]: Leaving directory '/pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake'
/usr/bin/gmake  -f src/CMakeFiles/myexe-hip.dir/build.make src/CMakeFiles/myexe-hip.dir/build
gmake[2]: Entering directory '/pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake'
[ 75%] Building HIP object src/CMakeFiles/myexe-hip.dir/main.cpp.o
cd /pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake/src && /appl/lumi/SW/CrayEnv/EB/rocm/4.5.2/llvm/bin/clang++ -D__HIP_ROCclr__=1 -I/pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/domain -I/pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/include -fPIC -fgpu-rdc -w -I/appl/lumi/SW/CrayEnv/EB/rocm/4.5.2/hip/include --gcc-toolchain=/opt/cray/pe/gcc/10.3.0/snos --offload-arch=gfx90a -mllvm -amdgpu-early-inline-all=true -mllvm -amdgpu-function-calls=false -std=gnu++17 -o CMakeFiles/myexe-hip.dir/main.cpp.o -x hip -c /pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/src/main.cpp
[100%] Linking HIP executable myexe-hip
cd /pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake/src && /pfs/lustrep4/users/jepiccin/bin/src/cmake-3.23.1-linux-x86_64/bin/cmake -E cmake_link_script CMakeFiles/myexe-hip.dir/link.txt --verbose=1
/appl/lumi/SW/CrayEnv/EB/rocm/4.5.2/hip/bin/hipcc_cmake_linker_helper /appl/lumi/SW/CrayEnv/EB/rocm/4.5.2/llvm/bin  -fPIC -fgpu-rdc -w -I/appl/lumi/SW/CrayEnv/EB/rocm/4.5.2/hip/include --gcc-toolchain=/opt/cray/pe/gcc/10.3.0/snos --offload-arch=gfx90a --hip-link "CMakeFiles/myexe-hip.dir/main.cpp.o" ../domain/CMakeFiles/test_obj.dir/test.cu.o ../domain/CMakeFiles/util_obj.dir/util.cu.o -o myexe-hip  /appl/lumi/SW/CrayEnv/EB/rocm/4.5.2/hip/lib/libamdhip64.so.4.4.40502 /pfs/lustrep3/appl/lumi/SW/CrayEnv/EB/rocm/4.5.2/llvm/lib/clang/13.0.0/lib/linux/libclang_rt.builtins-x86_64.a
gmake[2]: Leaving directory '/pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake'
[100%] Built target myexe-hip
gmake[1]: Leaving directory '/pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake'
/pfs/lustrep4/users/jepiccin/bin/src/cmake-3.23.1-linux-x86_64/bin/cmake -E cmake_progress_start /pfs/lustrep2/scratch/project_465000105/jg/DEL/20220729/my-gists.git/202112/build_cmake/CMakeFiles 0
#}}} 

OMP_NUM_THREADS=1 srun --gpus 1 -N1 -n1 -c1 -t1 --partition=eap -A project_465000105 ./build_cmake/src/myexe-hip
Results check passed!
#}}}





#{{{ cmake old

GNUJG=/opt/cray/pe/gcc/10.3.0
cmake -DCMAKE_BUILD_TYPE=Debug -S . -B build_clang13+hip \
-DCMAKE_C_COMPILER=$HIP_PATH/bin/hipcc \
-DCMAKE_CXX_COMPILER=$HIP_PATH/bin/hipcc \
-DCMAKE_CXX_FLAGS="--gcc-toolchain=$GNUJG/snos"

cmake --build build_clang13+hip -v

srun -Aproject_465000105 -peap -t1 -n1 -G1 build_clang13+hip/src/myexe-hip
# Results check passed!



hipcc --version
HIP version: 4.4.21432-f9dccde4
AMD clang version 13.0.0 (https://github.com/RadeonOpenCompute/llvm-project roc-4.5.2 21432 9bbd96fd1936641cd47defd8022edafd063019d5)
Target: x86_64-unknown-linux-gnu
Thread model: posix
InstalledDir: /appl/lumi/SW/CrayEnv/EB/rocm/4.5.2/llvm/bin

# add '#include "hip/hip_runtime.h"' in domain/util.cu
hipcc --offload-arch=gfx908 -fPIC -fgpu-rdc -std=c++17 -w -c domain/util.cu

hipcc --offload-arch=gfx908 -fPIC -fgpu-rdc -std=c++17 -w -c domain/test.cu -Iinclude

hipcc --offload-arch=gfx908  -fPIC -shared -dlink util.o test.o -o device_link.o -fgpu-rdc --hip-link

#no hipcc --offload-arch=gfx908 -fPIC -fgpu-rdc -std=c++17 src/main.cpp util.o test.o device_link.o -o exe -L$HIP_PATH/lib -lamdhip64 -I include/
/appl/lumi/SW/CrayEnv/EB/rocm/4.5.2/llvm/bin/clang++ -fPIC -fgpu-rdc -std=c++17 src/main.cpp util.o test.o device_link.o -o exe -L$HIP_PATH/lib -lamdhip64 -I include/

# hipcc -fPIC -shared -dlink util.o test.o -o device_link.o -fgpu-rdc --hip-link
# /opt/rocm/llvm/bin/clang++ src/main.cpp util.o test.o device_link.o -o exe -L/opt/rocm/hip/lib -lamdhip64 -Iinclude
#}}}
