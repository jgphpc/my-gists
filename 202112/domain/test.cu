#include "hip/hip_runtime.h"
#include <stdio.h>

#include "test.h"
#include "util.cuh"

#define DSIZE 1024
#define DVAL 10
#define SQVAL 3
#define nTPB 256

#define cudaCheckErrors(msg)                             \
  do {                                                   \
    hipError_t __err = hipGetLastError();              \
    if (__err != hipSuccess) {                          \
      fprintf(stderr, "Fatal error: %s (%s at %s:%d)\n", \
              msg, hipGetErrorString(__err),            \
              __FILE__, __LINE__);                       \
      fprintf(stderr, "*** FAILED - ABORTING\n");        \
      exit(1);                                           \
    }                                                    \
  } while (0)

__global__ void my_kernel(int *data){
  int idx = threadIdx.x + (blockDim.x *blockIdx.x);
  if (idx < DSIZE) data[idx] =+ DVAL + my_square (SQVAL);
}

int my_test_func()
{
  int *d_data, *h_data;
  h_data = (int *) malloc(DSIZE * sizeof(int));
  if (h_data == 0) {printf("malloc fail\n"); exit(1);}
  hipMalloc((void **)&d_data, DSIZE * sizeof(int));
  cudaCheckErrors("hipMalloc fail");
  for (int i = 0; i < DSIZE; i++) h_data[i] = 0;
  hipMemcpy(d_data, h_data, DSIZE * sizeof(int), hipMemcpyHostToDevice);
  cudaCheckErrors("hipMemcpy fail");
  hipLaunchKernelGGL(my_kernel, ((DSIZE+nTPB-1)/nTPB), nTPB, 0, 0, d_data);
  hipDeviceSynchronize();
  cudaCheckErrors("kernel");
  hipMemcpy(h_data, d_data, DSIZE * sizeof(int), hipMemcpyDeviceToHost);
  cudaCheckErrors("hipMemcpy 2");
  for (int i = 0; i < DSIZE; i++)
    if (h_data[i] != DVAL + SQVAL*SQVAL)
    {
      printf("Results check failed at offset %d, data was: %d, should be %d\n",
             i, h_data[i], DVAL);
      exit(1);
    }
  printf("Results check passed!\n");
  return 0;
}
