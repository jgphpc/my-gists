#include "hip/hip_runtime.h"
#include "util.cuh"

__device__ int my_square (int a)
{
  return a * a;
}
